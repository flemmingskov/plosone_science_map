# Python workflow for Science Mapping
**October 24, 2021**

Python scripts for science mapping
Author: Flemming Skov, fs@ecos.au.dk

This tool is developed for a study on science mapping for **PLOS ONE**: '*Science maps for exploration, navigation, and reflection  -	a graphic approach to strategic thinking*'. The tool consists of eight scripts for data capture, -cleaning, -analyses, overlay and visualization.

## Background
The world of science is growing at the scientific landscape is constantly changing as research specialties evolve, merge or become obsolete. Although science mapping is applied  to a wide range of scientific areas, examples of how to use these maps in a practical context are sparse. This paper shows how to use a topical, scientific reference maps to overview and navigate in dynamic research landscapes and how to use such maps as input to strategic thinking.

A workflow was written in Python for easy and fast retrieval of information for topic maps (including tokens from keywords section and title) to generate intelligible research maps, and to visualize the distribution of topics (keywords), papers, journal categories, individual researchers and research groups on any scale. The resulting projections revealed new insights into the structure of the research community and made it possible to compare researchers or research groups to describe differences and similarities, find scientific overlaps or gaps, and to see how they are related and connected.

A collection of articles authored by the current permanent staff at the two departments is the only input to the  process. The full process involves the following steps where each step corresponds to a python script
 - import of export text files from Web of Science and basic data cleansing
 - create a list of unique keyword phrases from author keywords and keyword plus
 - find and extract keyword phrases from title and abstract using the list created in step 2
 - build a co-occurrence matrix for keyword phrases
 - reduce dimensionality using FDP (force directed placement)
 - a tool to adjust and fine-tune the reference map for better presentation
 - prepare data for scientometric overlay mapping (here papers and authors are assigned coordinates in the map space)
 - a mapping module to create actual maps and images

## Instructions
### Install the program and necessary libraries
The scripts are compatible with Python vs. 3.6 and uses Streamlit as a front-end

 - Clone a copy of the repository to your computer or workspace
 - Install streamlit using pip (or install latest version from the streamlit.io)
 - Install required python libraries, including nlkt, sqlite3, pandas, matplotlib, seaborn, adjutsText and more (see complete list of dependencies in _toolmain.py_ 

 **IMPORTANT**: the file function_library.py contains the following function - it needs to be update to reflect your folder system and platform!

```
 def get_platform():
    if (platform.system() == 'Darwin'):
        mainwork = '/Users/nnnn/Desktop/nnnn/'
    else:
        mainwork = 'C:/Users/nnnn/nnnn/Desktop/'
    return mainwork
```


### Workspace set-up: folders and files
**Required folders**
 - All data should be kept in the main workspace (path/yourWorkSpace/). You can add the path to your workspaces in the config.sys file in the app-folder
 - The work space must include a 'data' folder where the raw .txt imports from Web of Science are stored
 - The program expects a 'figures' folder to store output
 - In some cases, where a data overlay is needed more 'data' folders may be added. Use, e.g., 'data_overlay1', etc to name them

```
 .../yourWorkSpace/
            |_data/
            |_figures/
            |_data_overlay1/. (optional)
```

**Files**

When the program runs it produces three sqlite databases and store them in the main workspace:
 - 'data.db' that contains imported (and later processed) bibliometric data from Web of Science
 - 'coor.db' with reference coordinates for all keywords in the specific landscape
 - 'map.db' that contain geo-referenced data for keywords, papers, persons and categories for the mapping

Generally the 'data.db' and 'map.db' should be used for the reference map. If overlays are needed (and to avoid confusion) name them (e.g.) 'data_overlay.db' and 'map_overlay.db'. See also folder structure above

The program expect a ‘config.txt’ file with suggested names for your databases:

 - default_data_db = 'data_XXXX.db'
 - default_coordinates_db = 'coordinates_XXXX.db'
 - default_map_db = 'map_XXXX.db'


### How to use the app
Start app from a terminal-window typing: "streamlit run 'path'/tool_main.py
Instructions for use and help are included in the app

